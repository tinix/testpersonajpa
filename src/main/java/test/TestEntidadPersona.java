package test;

import static org.junit.Assert.assertTrue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import beans.dominio.Persona;

public class TestEntidadPersona {

	static EntityManager em = null;
	static EntityTransaction tx = null;
	static EntityManagerFactory emf = null;
	Logger log = LogManager.getRootLogger();


	@BeforeClass
	public static void setup() {
		try {
			emf = Persistence.createEntityManagerFactory("PersonaPU");
			em = emf.createEntityManager();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void testPersonaEntity() {
		log.debug("Iniciando test Persona Entity con JPA");
		assertTrue(em != null);

		EntityTransaction tx = em.getTransaction();
		tx.begin(); //comienza la transaccion

		// No se debe especificar el ID ya que se genera en automatico
		Persona persona1 = new Persona("Oscar", "Gomez", "Larios", "ogomez@gmail.com", "4567890");
		
		em.persist(persona1);
		tx.commit(); // termina la transaccion
		
		assertTrue(persona1.getIdPersona() != null);
		log.debug("Objeto persistido :" + persona1);
		log.debug("Fin de test Persona Entity con JPA");
	}
	
	@After
	public void tearDown() throws Exception {
		if(em != null) {
			em.close();
		}
	}
}